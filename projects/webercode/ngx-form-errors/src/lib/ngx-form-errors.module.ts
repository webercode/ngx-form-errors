import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxFormErrorsComponent } from './ngx-form-errors.component';

@NgModule({
  declarations: [
    NgxFormErrorsComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    NgxFormErrorsComponent,
  ],
})
export class NgxFormErrorsModule {}
