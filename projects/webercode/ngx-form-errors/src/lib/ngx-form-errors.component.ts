import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'ngx-form-errors',
  template: `
    <ng-container *ngIf="errors">
      <ng-container *ngIf="separator">
        {{ this.errorStrings.join(separator) }}
      </ng-container>
      <ng-container *ngIf="!separator">
        <ng-container *ngFor="let error of errorStrings; let i = index">
          <br *ngIf="i">
          {{ error }}
        </ng-container>
      </ng-container>
    </ng-container>
  `,
  styles: [],
})
export class NgxFormErrorsComponent implements OnChanges {

  @Input() errors: null|{ [i: string]: any };

  errorStrings: string[] = [];

  @Input() separator: string;

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('errors' in changes) {
      this.errorStrings = this.errors ? Object.values(this.errors).filter(e => typeof e === 'string' && !!e) : [];
    }
  }

}
