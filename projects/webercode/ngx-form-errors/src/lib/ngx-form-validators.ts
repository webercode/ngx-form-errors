import { FormControl, ValidatorFn, Validators } from '@angular/forms';

export class NgxFormValidators {

  static required(opts: { error?: string } = {}): ValidatorFn {

    const fn = (control: FormControl) => {
      const errors = Validators.required(control);
      return !errors ? null : { required: opts.error || 'an input is required' };
    };

    return fn;
  }

  static requiredTrue(opts: { error?: string } = {}): ValidatorFn {

    const fn = (control: FormControl) => {
      const errors = Validators.requiredTrue(control);
      return !errors ? null : { required: opts.error || 'input is not true' };
    };

    return fn;
  }

  static email(opts: { error?: string } = {}): ValidatorFn {

    const fn = (control: FormControl) => {
      const errors = Validators.email(control);
      return !errors ? null : { email: opts.error || `invalid email "${control.value}"` };
    };

    return fn;
  }

  static max(opts: { value: number, error?: string }): ValidatorFn {

    const fn = (control: FormControl) => {
      const errors = Validators.max(opts.value)(control);
      return !errors ? null : { max: opts.error || `${control.value} is not ≤ ${opts.value}` };
    };

    return fn;
  }

  static min(opts: { value: number, error?: string }): ValidatorFn {

    const fn = (control: FormControl) => {
      const errors = Validators.min(opts.value)(control);
      return !errors ? null : { min: opts.error || `${control.value} is not ≥ ${opts.value}` };
    };

    return fn;
  }

  static maxLength(opts: { value: number, error?: string }): ValidatorFn {

    const fn = (control: FormControl) => {
      const errors = Validators.maxLength(opts.value)(control);
      return !errors ? null :
        { maxLength: opts.error || `input length of ${control.value.length} is not ≤ ${opts.value}` };
    };

    return fn;
  }

  static minLength(opts: { value: number, error?: string }): ValidatorFn {

    const fn = (control: FormControl) => {
      const errors = Validators.minLength(opts.value)(control);
      return !errors ? null :
        { minLength: opts.error || `input length of ${control.value.length} is not ≥ ${opts.value}` };
    };

    return fn;
  }

  static pattern(opts: { value: string|RegExp, error?: string }): ValidatorFn {

    const quoteMark = typeof opts.value === 'string' ? '"' : '';

    const fn = (control: FormControl) => {
      const errors = Validators.pattern(opts.value)(control);
      return !errors ? null :
        { minLength: opts.error || `"${control.value}" does not match ${quoteMark}${opts.value}${quoteMark}` };
    };

    return fn;
  }

}
