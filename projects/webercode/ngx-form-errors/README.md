# WeberCode NgxFormErrorsModule

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

To quickly get started using this library, follow these steps:

1. Run `npm i @webercode/ngx-form-errors` inside your project.
2. Import the `NgxFormErrorsModule` where appropriate.
3. Import the `ImprovedValidators` class which expands and improves on the default `Validators` class shipped with `@angular/forms` and use
   these validators in your forms.
4. In your template use `<ngx-form-errors></ngx-form-errors>` provided by the `NgxFormErrorsComponent` to display your errors.

## Features

Below are details on the symbols exported by this library.

### ImprovedValidators

All of the validators attached to the `Validators` class from `@angular/forms` have been implemented here except that they take more
options than the original validators and return errors with a generic string messages. All validators can be handed options that, at a
minimum, have the following signature: `opts: { error?: string }`. If you don't like the default message, you can override it using this
property.

> NOTE: `error?: string` may eventually change to something like `error?: string|(v: any, opts: any) => string`.

The following validators have been improved: `required`, `requiredTrue`, `email`, `max`, `min`, `maxLength`, `minLength`, and `pattern`. 

### NgxFormErrorsComponent

To use the `NgxFormErrorsComponent` in a template, make sure you have imported `NgxFormErrorsModule` appropriately. Then, in your template:

```html
<ngx-form-errors [errors]="formControl.errors" [separator]="', '"></ngx-form-errors>
```

The default behavior is to list all the string errors on the form control errors object with a single `<br>` in between. If you would
instead like the errors to be separated by a string of some sort, use the `[separator]` input. E.g. for comma separated errors, use
`separator=", "`.

> NOTE: no special `<div>`'s or any other such tags have been used (except for `<br>`) so as not to interefere with a project's custom CSS/SCSS.
> If this doesn't fit your purpose, you are free to look at the source code, extract the idea, and implement it in your own way! Or, submit
> a PR with the suggested change and I'll try to take a look at it as soon as possible =)
