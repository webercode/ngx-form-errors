import { Component } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { NgxFormValidators } from '@webercode/ngx-form-errors';

@Component({
  selector: 'app-better-validation-errors',
  template: `
    <div class="flex-container mt-3">
      <div class="row">

        <ng-container *ngFor="let e of examples">

          <div class="col-4">
            <h3>
              <label [for]="e.title">{{ e.title }}</label>
            </h3>
            <input [id]="e.title" [formControl]="e.ctrl">
            <br><br>
            <p>Errors:</p>
            <pre *ngIf="e.ctrl.errors">{{ e.ctrl.errors | json }}</pre>
          </div>

        </ng-container>

      </div>
    </div>
  `
})
export class BetterValidationErrorsComponent
{

  examples: { title: string, ctrl: FormControl }[] = [
    {
      title: 'NgxFormValidators.required()',
      ctrl: this.formBuilder.control(null, NgxFormValidators.required())
    },
    {
      title: 'NgxFormValidators.email()',
      ctrl: this.formBuilder.control('not an email', NgxFormValidators.email())
    },
    {
      title: 'NgxFormValidators.max({ value: 0 })',
      ctrl: this.formBuilder.control(.001, NgxFormValidators.max({ value: 0 }))
    },
    {
      title: 'NgxFormValidators.min({ value: 0 })',
      ctrl: this.formBuilder.control(-.001, NgxFormValidators.min({ value: 0 }))
    },
    {
      title: 'NgxFormValidators.maxLength({ value: 5 })',
      ctrl: this.formBuilder.control('longer than 5 letters', NgxFormValidators.maxLength({ value: 5 }))
    },
    {
      title: 'NgxFormValidators.minLength({ value: 5 })',
      ctrl: this.formBuilder.control('four', NgxFormValidators.minLength({ value: 5 }))
    },
    {
      title: 'NgxFormValidators.pattern({ value: /abc/ })',
      ctrl: this.formBuilder.control('a', NgxFormValidators.pattern({ value: /abc/ }))
    }
  ];

  constructor(
    private formBuilder: FormBuilder
  )
  {
  }

}
