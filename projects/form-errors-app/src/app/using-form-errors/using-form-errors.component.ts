import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgxFormValidators } from '@webercode/ngx-form-errors';

@Component({
  selector: 'app-using-form-errors',
  template: `
    <div class="flex-container mt-3">
      <div class="row">

        <div class="col">
          <label for="mixed-example">Input</label>
          <br>
          <input id="mixed-example" [formControl]="textInput">
          <br>
          <br>
          <ngx-form-errors [errors]="textInput.errors" [separator]="separator.value"></ngx-form-errors>
        </div>
        <div class="col">
          <button (click)="NextInput()">Next Input</button>
          <br>
          <br>
          <button (click)="NextSeparator()">Next Separator</button>
          <br>
          <br>
          <label for="separator">Separator: </label>
          <br>
          <input id="separator" [formControl]="separator">
        </div>

      </div>
    </div>
  `,
})
export class UsingFormErrorsComponent {

  separator = this.formBuilder.control(null);

  sepIndex = 0;

  separators = [null, ', ', '; ', '. '];

  inputIndex = 0;

  inputs = [null, 'email@example.com', .001, 0, -.001, 'longer than 5 letters', 'four', 'abc'];

  textInput = this.formBuilder.control(null, [
    NgxFormValidators.required(),
    NgxFormValidators.email(),
    NgxFormValidators.max({ value: 0 }),
    NgxFormValidators.min({ value: 0 }),
    NgxFormValidators.maxLength({ value: 5 }),
    NgxFormValidators.minLength({ value: 5 }),
    NgxFormValidators.pattern({ value: /abc/ }),
  ]);

  constructor(
    private formBuilder: FormBuilder,
  ) {}

  NextInput() {
    let i = this.inputIndex + 1;

    if (i === this.inputs.length) {
      i = 0;
    }

    this.textInput.setValue(this.inputs[this.inputIndex = i]);
    this.textInput.updateValueAndValidity();
  }

  NextSeparator() {
    let i = this.sepIndex + 1;

    if (i === this.separators.length) {
      i = 0;
    }

    this.separator.setValue(this.separators[this.sepIndex = i]);
    this.separator.updateValueAndValidity();
  }

}
