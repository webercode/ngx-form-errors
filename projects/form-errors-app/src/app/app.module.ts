import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgxFormErrorsModule } from '@webercode/ngx-form-errors';
import { AppComponent } from './app.component';
import { BetterValidationErrorsComponent } from './better-validation-errors/better-validation-errors.component';
import { DefaultAngularValidatorsComponent } from './default-angular-validators/default-angular-validators.component';
import { UsingFormErrorsComponent } from './using-form-errors/using-form-errors.component';

// noinspection AngularInvalidImportedOrDeclaredSymbol
@NgModule({
  declarations: [
    AppComponent,
    DefaultAngularValidatorsComponent,
    BetterValidationErrorsComponent,
    UsingFormErrorsComponent,
  ],
  imports: [
    BrowserModule,
    NgxFormErrorsModule,
    ReactiveFormsModule,
    NgxFormErrorsModule,
    NgxFormErrorsModule,
  ],
  providers: [],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule {}
