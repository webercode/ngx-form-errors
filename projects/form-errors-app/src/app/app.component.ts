import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  title = 'the ngx-form-errors demonstration app';

  showTraditionalExamples = false;

  showBetterExamples = false;

  showComponentUsage = false;
}
