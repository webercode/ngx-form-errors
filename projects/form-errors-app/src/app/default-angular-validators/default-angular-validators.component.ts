import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-default-angular-validators',
  template: `
    <div class="flex-container mt-3">
      <div class="row">

        <ng-container *ngFor="let e of examples">

          <div class="col-3">
            <h3>
              <label [for]="e.title">{{ e.title }}</label>
            </h3>
            <input [id]="e.title" [formControl]="e.ctrl">
            <br><br>
            <p>Errors:</p>
            <pre *ngIf="e.ctrl.errors">{{ e.ctrl.errors | json }}</pre>
          </div>

        </ng-container>

      </div>
    </div>
  `,
})
export class DefaultAngularValidatorsComponent {

  examples: { title: string, ctrl: FormControl }[] = [
    {
      title: 'Validators.required',
      ctrl: this.formBuilder.control(null, Validators.required),
    },
    {
      title: 'Validators.email',
      ctrl: this.formBuilder.control('not an email', Validators.email),
    },
    {
      title: 'Validators.max(0)',
      ctrl: this.formBuilder.control(.001, Validators.max(0)),
    },
    {
      title: 'Validators.min(0)',
      ctrl: this.formBuilder.control(-.001, Validators.min(0)),
    },
    {
      title: 'Validators.maxLength(5)',
      ctrl: this.formBuilder.control('longer than 5 letters', Validators.maxLength(5)),
    },
    {
      title: 'Validators.minLength(5)',
      ctrl: this.formBuilder.control('four', Validators.minLength(5)),
    },
    {
      title: 'Validators.pattern(/abc/)',
      ctrl: this.formBuilder.control('a', Validators.pattern(/abc/)),
    },
  ];

  constructor(
    private formBuilder: FormBuilder,
  ) {}

}
